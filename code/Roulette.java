import java.util.*;
public class Roulette {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        RouletteWheel rw = new RouletteWheel();
        int playerMoney = 1000;
        System.out.println("Would you like to place a bet?");
            String yesOrNo = reader.next();
            if (yesOrNo.equals("no")) {
                System.out.println("Sorry, you must place a bet to play!");
            }
        while (true) {
            System.out.println("How much would you like to stake?");
            int stakeAmount = reader.nextInt();
            if (stakeAmount < 0 || stakeAmount > playerMoney) { 
                throw new IllegalArgumentException("The amount you are trying to stake is impossible!");
            }
            playerMoney = playerMoney - stakeAmount;
            System.out.println("What number are you betting on?");
            int playerNum = reader.nextInt();
            rw.spin();
            if (playerNum == rw.getValue()) {
                System.out.println("You WON!!");
                int temp = stakeAmount * playerNum;
                playerMoney = playerMoney + temp;
            } else {
                System.out.println("You lost! 99% of gamblers quit before winning big!");
                System.out.println("The ball landed on "+rw.getValue());
            }
            System.out.println("Would you like to keep playing?");
            String keepPlaying = reader.next();
            if (keepPlaying.equals("no")) {
                break;
            }
        }
    }
}

