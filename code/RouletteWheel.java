import java.util.Random;
public class RouletteWheel {
    
    private Random rand;
    private int number;

    public RouletteWheel() {
        this.rand = new Random();
        this.number = 0;
    }

    public void spin() {
        this.number = this.rand.nextInt(37);
    }

    public int getValue() {
        return this.number;
    }
}


